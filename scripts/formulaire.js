
let error = []
const form = document.getElementById('formulaire')
const prenom = document.getElementById('prenom')
const nom = document.getElementById('nom')
const email = document.getElementById('courriel')
const emailValide = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
const telephone = document.getElementById('telephone')
const telValide = /^[0-9]{3}-[0-9]{3}-[0-9]{4}$/
const service = document.getElementById('service')
const commentaire = document.getElementById('commentaire')
const errorContainer = document.querySelector('.error-list')
const chaine_a_valider = [prenom, nom]
const errorField = document.getElementById('error-field')

form.addEventListener('submit', function(e){
    errorField.textContent = " "
    error = []
    validateStringInput()
    validateEmailInput()
    validateTelephone()
    validateService()
    validateTextArea()

    if(error.length > 0){
        errorContainerManager()
        e.preventDefault()
    }
})

function validateStringInput(){
    for(let i = 0; i < chaine_a_valider.length; i++){
        let chaine = chaine_a_valider[i]
        if (chaine.value.length <= 0){
            error.push(`Vous devez écrire un ${chaine.id}`)
            errorCaller(chaine)
        } else {
            errorRemove(chaine)
        }
    }
}

function errorCaller(champ){
    champ.classList.add('input-error')
}

function errorRemove(champ){
    champ.classList.remove('input-error')
}

function validateEmailInput(){
    if(email.value.length <= 0){
        error.push(`Veillez écrire un ${email.id}`)
        errorCaller(email)
    }
    if(!emailValide.test(email.value)){
        error.push(`Le ${email.id} doit être d'un format valide`)
        errorCaller(email)
    } else {
        errorRemove(email)
    }
}

function validateTelephone(){
    if(telephone.value.length <= 0){
        error.push(`Veillez écrire un ${telephone.id}`)
        errorCaller(telephone)
    }
    if(!telValide.test(telephone.value)){
        error.push(`Le ${telephone.id} doit être d'un format valide(XXX-XXX-XXXX)`)
        errorCaller(telephone)
    } else {
        errorRemove(telephone)
    }
}

function validateService(){
    if(service.value == '') {
        error.push(`Veillez choisir un ${service.id}`)
        errorCaller(service)
    } else {
        errorRemove(service)
    }
}

function validateTextArea(){
    let spaceless = commentaire.value.replace(/\s/g, '')
    if (spaceless.length <= 0){
        error.push(`Veillez spécifier vos besoins`)
        errorCaller(commentaire)
    } else {
      errorRemove(commentaire)
    }
}

function errorContainerManager(){
    for (err in error){
        let tag = document.createElement('p')
        let error_text = document.createTextNode(error[err])
        tag.appendChild(error_text)
        errorContainer.appendChild(tag)
        errorContainer.classList.add('error-list-visible')
    }
}